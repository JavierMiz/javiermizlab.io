import { Link } from "gatsby"
import React from "react"

import "../../sass/components/botones.scss"

type valores = {
  fill: "outline" | "solid"
  enlace: string
  texto: string
}

const BotonSecundario: React.FC<valores> = props => {
  const claseBoton = "boton boton--secundario boton--" + props.fill

  return (
    <Link to={props.enlace} className={claseBoton}>
      <span>{props.texto}</span>
    </Link>
  )
}

export default BotonSecundario
