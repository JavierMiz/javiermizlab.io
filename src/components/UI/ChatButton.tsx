import React from "react"

import "../../sass/components/chat-button.scss"
import { Link } from "gatsby"

const ChatButton: React.FC = () => {
  return (
      <Link to="https://wa.me/528261431778?text=Me%20interesa%20trabajar%20contigo" className="chat-button" target="_blank">
        <img src="/icono-whatsapp.svg" alt="" />
      </Link>
  )
}

export default ChatButton
