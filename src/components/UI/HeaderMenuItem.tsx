import { Link } from "gatsby"
import React from "react"

type valores = {
  enlace: string,
  texto: string,
}

const HeaderMenuItem: React.FC<valores> = (props) => {

  return (
    <Link to={props.enlace} className="header__menu__item" activeClassName="activo">
      {props.texto}
    </Link>
  );
}

export default HeaderMenuItem;