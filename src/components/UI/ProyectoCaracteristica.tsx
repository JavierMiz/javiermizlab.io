import { Link } from "gatsby"
import React from "react"

import "../../sass/components/project-feature.scss"

type valores = {
  items: string[][]
}

const Features: React.FC<valores> = (props) => {
  const caracteristicas = props.items;

  const listaFeatures = caracteristicas.map((caracteristica, index) => (
    <div key={index} className="project-feature">
      <h6 className="project-feature__titulo">{caracteristica[0]}</h6>
      <p className="project-feature__descripcion">
        {caracteristica[1]}
      </p>
    </div>
  ))

  return (
    <div className="project-feature-container">
      {listaFeatures}
    </div>
  )
}

export default Features
