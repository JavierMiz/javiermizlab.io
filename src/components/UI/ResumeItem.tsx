import { Link } from "gatsby"
import React from "react"

import "../../sass/components/resume-item.scss"

type valores = {
  titulo: string
  subtitulo: string
  fecha?: string
  descripcion?: string
  enlace?: string
  enlaceTexto?: string
}

const ResumeItem: React.FC<valores> = props => {
  return (
    <div className="resume__item">
      <h3 className="resume__item__titulo">{props.titulo}</h3>
      <h4 className="resume__item__subtitulo">{props.subtitulo}</h4>
      {props.fecha && <span className="resume__item__fecha">{props.fecha}</span>}
      {props.descripcion && <p className="resume__item__descripcion">{props.descripcion}</p>}
      {props.enlace && <Link to={props.enlace} className="resume__item__enlace">{props.enlaceTexto}</Link>}
    </div>
  )
}

export default ResumeItem
