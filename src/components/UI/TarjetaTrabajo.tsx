import React from "react"

import "../../sass/components/tarjetas-trabajo.scss"

type valores = {
  color: string
  imagen: string
  titulo: string
  descripcion?: string
  enlace?: string
}

const TarjetaTrabajo: React.FC<valores> = props => {
  const tarjetaStyle = {
    boxShadow: `-5px -5px 0px ${props.color}`,
  }

  const tarjetaImagen = {
    backgroundImage: `url(/sites-screenshots/${props.imagen})`,
  }

  const botonStyles = {
    color: `${props.color}`,
  }

  return (
    <div className="tarjeta-trabajo" style={tarjetaStyle}>
      <div className="tarjeta-trabajo__imagen" style={tarjetaImagen}></div>
      <div className="tarjeta-trabajo__cuerpo">
        <h4 className="tarjeta-trabajo__cuerpo__titulo">{props.titulo}</h4>
        {props.descripcion && (
          <p className="tarjeta-trabajo__cuerpo__descripcion">
            {props.descripcion}
          </p>
        )}
        {props.enlace ? (
          <a href={props.enlace} target="_blank" className="boton" style={botonStyles}>
            Visitar web
          </a>
        ) : (
          <span className="boton boton--disabled">Web no disponible</span>
        )}
      </div>
    </div>
  )
}

export default TarjetaTrabajo
