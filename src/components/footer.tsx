import { Link } from "gatsby"
import React from "react"
import Obfuscate from "react-obfuscate"

import "../sass/components/footer.scss"

const Footer: React.FC = () => {
  return (
    <>
      <h3 className="footer-titulo">Contáctame</h3>
      <footer className="footer">
        <div className="contenedor contenedor--flex">
          <div className="footer__col">
            <h6 className="footer__col__titulo">Correo o teléfono</h6>
            <div className="contacto-item">
              <img
                className="contacto-item__icono"
                src="/icono-email.svg"
                alt=""
              />
              <Obfuscate
                email="jmizarevalo@gmail.com"
                headers={{
                  cc: "web@javiermiz.gitlab.io",
                  subject: "Quiero trabajar contigo",
                  body: "Este soy yo y mi proyecto:",
                }}
              />
            </div>
            <div className="contacto-item">
              <img
                className="contacto-item__icono"
                src="/icono-phone.svg"
                alt=""
              />
              <Obfuscate tel="8261431778" />
            </div>
          </div>
          <div className="footer__col">
            <h6 className="footer__col__titulo">Redes de trabajo:</h6>
            <div className="contacto-item">
              <img
                className="contacto-item__icono"
                src="/icono-facebook.svg"
                alt=""
              />
              <Link
                target="_blank"
                to="https://www.facebook.com/tecnologoslatam"
              >
                Tecnólogos Facebook
              </Link>
            </div>
            {/* <div className="contacto-item">
              <img
                className="contacto-item__icono"
                src="/icono-instagram.svg"
                alt=""
              />
              <Link target="_blank" to="https://www.facebook.com/tecnologospls">
                Tecnólogos Instagram
              </Link>
            </div> */}
            <div className="contacto-item">
              <img
                className="contacto-item__icono"
                src="/icono-whatsapp.svg"
                alt=""
              />
              <Link
                target="_blank"
                to="https://wa.me/528261431778?text=Me%20interesa%20trabajar%20contigo"
              >
                Tecnólogos Whatsapp
              </Link>
            </div>
          </div>
          <div className="footer__col">
            <h6 className="footer__col__titulo">Redes personales:</h6>
            <div className="contacto-item">
              <img
                className="contacto-item__icono"
                src="/icono-facebook.svg"
                alt=""
              />
              <Link target="_blank" to="https://www.facebook.com/javimiz">
                Facebook
              </Link>
            </div>
            <div className="contacto-item">
              <img
                className="contacto-item__icono"
                src="/icono-instagram.svg"
                alt=""
              />
              <Link target="_blank" to="https://www.instagram.com/javitl_/">
                Instagram
              </Link>
            </div>
          </div>
        </div>
        <div className="footer__copyright">
          © {new Date().getFullYear()}, Página desarrollada por Javier Miz Arévalo
        </div>
      </footer>
    </>
  )
}

export default Footer
