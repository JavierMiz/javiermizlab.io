import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { createRef, useEffect, useState } from "react"

import HeaderMenuItem from "./UI/HeaderMenuItem"

import "../sass/normalize.scss"
import "../sass/general.scss"
import "../sass/components/header.scss"

import atras from "../images/icono-flecha-atras.png"

const Header = ({ siteTitle }) => {
  const [url, setUrl] = useState("/");

  useEffect(() => {
    setUrl(window.location.pathname);
  },[]);
  
  let showBackButton = url == "/" ? false : true

  return (
    <header className="header">
      <div className="contenedor">
        <nav className="header__nav">
          <Link to="/" className="header__titulo">
            {showBackButton ? (
              <>
                <img src={atras} alt="" />
                <span>Inicio</span>
              </>
            ) : (
              <h2>Portafolio</h2>
            ) }
          </Link>
          <div className="header__menu">
            <HeaderMenuItem enlace="/acerca" texto="Sobre mi" />
            <HeaderMenuItem enlace="/trabajos" texto="Trabajos" />
          </div>
        </nav>
      </div>
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
