import React from "react"

import ilustracion from "../../images/ilustracion-hero.png"
import flechaAbajo from "../../images/icono-flecha-abajo.svg"
import Boton from "../UI/Boton"

import "../../sass/components/hero.scss"

const Hero: React.FC = () => {
  return (
    <div className="hero">
      <div className="contenedor hero__contenedor">
        <div className="hero__contenido">
          <h1>Hola, soy Javier Miz!</h1>
          <p>Programador y desarrollador web y de aplicaciones Front-end 👌</p>
          <Boton enlace="https://wa.me/528261431778?text=Me%20interesa%20trabajar%20contigo" fill="outline" texto="Trabajemos juntos" />
        </div>
        <div className="hero__ilustracion">
          <img src={ilustracion} alt="" />
        </div>
      </div>
      <a href="#sobre-mi" className="hero__bajar">
        Ver más
        <img src={flechaAbajo} alt="" />
      </a>
    </div>
  )
}

export default Hero;