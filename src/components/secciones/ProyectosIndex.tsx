import React from "react"

import TarjetaTrabajo from "../UI/TarjetaTrabajo"
import Boton from "../UI/Boton"

type valores = {
  show: number
}

const SobreMi: React.FC<valores> = props => {
  return (
    <div className="seccion">
      <div className="contenedor">
        <h2 className="contenedor__titulo">En qué he trabajado</h2>
        <div className="tarjetas-trabajos">
          {props.show >= 1 && (
            <TarjetaTrabajo
              color="#FEA100"
              imagen="airmobil.jpg"
              titulo="Airmobil"
              descripcion="Web para ofrecer servicios de perforaciones de pozos y excavaciones desde la que se puede pedir una cotización por medio de formularios. Sitio desarrollado con WordPress para aumentar las ventas de mi padre con buenos resultados"
              enlace="http://airmobil.com.mx/"
            />
          )}
          {props.show >= 2 && (
            <TarjetaTrabajo
              color="#f0b7b3"
              imagen="bolsaskaty.jpg"
              titulo="Bolsas Katy"
              descripcion="Desarrollo web de página estílo e-commerce para venta de bolsas que enlaza los productos con Amazon, gracias a los afiliados a Amazon. Sitio desarrollado con WordPress como proyecto personal que actualmente no está activo"
              enlace="http://www.bolsaskaty.com"
            />
          )}
          {props.show >= 3 && (
            <TarjetaTrabajo
              color="#FF6E3B"
              imagen="la-jarochita.jpg"
              titulo="La Jarochita"
              descripcion="Desarrollo de rediseño del sitio de un comercio de comida partiendo de un diseño trabajado por otra persona. Maqueté completamente el HTML, CSS y Javascript para pasarlo después a WordPress con PHP y así facilitar su administración"
              enlace="http://www.lajarochita.mx"
            />
          )}
          {props.show >= 4 && (
            <TarjetaTrabajo
              color="#9D489B"
              imagen="clinica-vida-fertil.jpg"
              titulo="Clínica vida fértil"
              descripcion="Desarrollo para ofrecer servicios de clínica y facilitar el contácto por correo o WhatsApp al igual que ofrecer la información en inglés y español. Sitio desarrollado con WordPress junto con otras 2 personas."
              enlace="https://clinicavidafertil.com/es/"
            />
          )}
          <Boton fill="outline" enlace="/trabajos" texto="Ver todos" />
        </div>
      </div>
    </div>
  )
}

export default SobreMi
