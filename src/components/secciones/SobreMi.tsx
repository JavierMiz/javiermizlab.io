import React from "react"


import Obfuscate from "react-obfuscate"
import BotonSecundario from "../UI/BotonSecundario"

import Yo from "../../images/avatar.gif"

const SobreMi: React.FC = () => {
  return (
    <div id="sobre-mi" className="seccion">
      <div className="contenedor contenedor--flex">
        <img src={Yo} alt="" className="imagen-circular" />
        <div className="tarjeta-sobre-mi">
          <h2>Quien soy 🤔</h2>
          <p>
            Soy un joven graduado en Ingeniería en sistemas computacionales. Me
            gusta hacer y desarrollar proyectos y trabajos de calidad. Disfruto
            desarrollar soluciones para clientes con negocios que van iniciando
            y así facilitarles su crecimiento. Me gusta mi trabajo y soy feliz
            desarrollando para mejorar la vida de todos
          </p>
          <p>
            <blockquote>
              <i>
                "Mas buscad primeramente el reino de Dios y su justicia, y todas
                estas cosas os serán añadidas."
              <br />
              - Mateo 6:33
              </i>
            </blockquote>
          </p>
          <p>
            <b>Contáctame:</b>
          </p>
          <ul>
            <li>
              <Obfuscate
                email="jmizarevalo@gmail.com"
                headers={{
                  cc: "web@javiermiz.gitlab.io",
                  subject: "Quiero trabajar contigo",
                  body: "Este soy yo y mi proyecto:",
                }}
              />
            </li>
            <li>
              <Obfuscate tel="8261353901" />
            </li>
          </ul>
          <BotonSecundario fill="solid" enlace="/acerca" texto="Más sobre mi" />
        </div>
      </div>
    </div>
  )
}

export default SobreMi;