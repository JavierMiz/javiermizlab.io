import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import ProyectosIndex from "../components/secciones/ProyectosIndex"
import Obfuscate from "react-obfuscate"

import Yo from "../images/avatar.jpg"

import "../sass/views/acerca.scss"
import ResumeItem from "../components/UI/ResumeItem"

const Acerca: React.FC = () => (
  <Layout>
    <SEO title="Sobre Javier Miz Arévalo" />
    <div className="page-wrapper resume">
      <div className="seccion">
        <div className="contenedor resume__header">
          <img src={Yo} alt="" className="resume__header__imagen" />
          <div className="resume__header__info">
            <h1>Javier Miz Arévalo</h1>
            <h2>Desarrollador Web Front-End</h2>
            <p>
              Soy un joven graduado en Ingeniería en sistemas computacionales.
              Me gusta hacer y desarrollar proyectos y trabajos de calidad.
              Disfruto desarrollar soluciones para clientes con negocios que van
              iniciando y así facilitarles su crecimiento. Me gusta mi trabajo y
              soy feliz desarrollando para mejorar la vida de todos
            </p>
            <p>
              <i>Santa Catarina Nuevo León, México</i>
            </p>
            <p>
              <Obfuscate
                email="jmizarevalo@gmail.com"
                headers={{
                  cc: "web@javiermiz.gitlab.io",
                  subject: "Quiero trabajar contigo",
                  body: "Este soy yo y mi proyecto:",
                }}
              />
              <br />
              <Obfuscate tel="8261353901" />
            </p>
          </div>
        </div>
      </div>
      <div className="seccion seccion--clara">
        <div className="contenedor">
          <div className="resume__bloque">
            <h2>Educación</h2>
            <ResumeItem
              titulo="Universidad de Montemorelos"
              subtitulo="Ingeniería en Sistemas computacionales"
              fecha="Agosto 2015 - Mayo 2019"
            />
          </div>
          <div className="resume__bloque">
            <h2>Experiencia</h2>
            <ResumeItem
              titulo="Nett"
              subtitulo="Desarrollador web Front-end"
              fecha="Julio 2018 - Julio 2020"
            />
            <ResumeItem
              titulo="Neomediax"
              subtitulo="Desarrollador web"
              fecha="May 2019 - October 2019"
            />
          </div>
          <div className="resume__bloque">
            <h2>Proyectos</h2>
            <ResumeItem
              titulo="Este portafolio"
              subtitulo="Web con Gatsby y React"
              descripcion="Me propuse aprender React como nueva tecnología y me ha gustado muchísimo. Leyendo un poco encontré que con GatsbyJs que hace uso de React podría desarrollar un sitio estático y desplegarlo online sin mucho problema y aquí estamos"
            />
            <ResumeItem
              titulo="Clasificación de imágenes con Tensorflow"
              subtitulo="Web con uso de Machine Learning"
              descripcion="Como proyecto de la universidad, desarrollé una pagina web con Flask (Python) que comunica el input de la imagen de una piedra con un modelo entrenado de machine learning con Tensorflow y devuelve el nombre del tipo de roca detectado."
              enlace="https://github.com/javitl/Image-classify-with-tensorflow-and-web-visualization"
              enlaceTexto="Repositorio del proyecto"
            />
            <ResumeItem
              titulo="Tecnólogos"
              subtitulo="Blog personal"
              descripcion="Actualmente cerrado como blog, desarrollé un sitio web por mi cuenta, este fue mi primer proyecto en desarrollo web. Empecé el desarrollo de un blog personal de noticias para Blogger con un template, y después de aprender poco a poco HTML, CSS y JavaScript, volví a empezar todo para desarrollarlo desde cero y así después pasarlo a WordPress con PHP con éxito"
              enlace="https://github.com/javitl/Tecnologos-web-template-bootstrap"
              enlaceTexto="Repositorio del proyecto"
            />
            <ResumeItem
              titulo="FITEC"
              subtitulo="Web para la facultad de la Universidad de Montemorelos"
              descripcion="Desarrollé el sitio de la facultad en donde estudié, sin diseño previo me metí a armar el HTML, CSS y JavaScript del sitio completo, para hacerlo administrable lo pasé a WordPress con PHP y con ayuda de un profesor (Daniel Gutierrez) he ido reparando los errores que han surgido y de los cuales no tenía conocimiento cuando desarrollé este proyecto"
              enlace="http://fit.um.edu.mx/"
              enlaceTexto="Ir a la página"
            />
            <ResumeItem
              titulo="Sitios con WordPress"
              subtitulo="Proyectos que he realizado si tener que armar todo el maquetado"
              descripcion="A lo largo del tiempo he trabajado varios sitios con WordPress, entre mis favoritos están Airmobil que es el sitio para la empresa de mi papá y que ha dado resultados positívos, BolsasKaty que fué un proyecto de ecommerce que empecé con mi novia y me divertí haciendo todo sin tener un diseño aunque esté ináctivo por falta de tiempo"
            />
          </div>
        </div>
      </div>
    </div>
    <ProyectosIndex show={2} />
  </Layout>
)

export default Acerca
