import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

import "../sass/views/index.scss"

import Hero from "../components/secciones/Hero"
import SobreMi from "../components/secciones/SobreMi"
import ProyectosIndex from "../components/secciones/ProyectosIndex"


const IndexPage: React.FC = () => (
  <Layout>
    <SEO title="Javier Miz Arévalo | Desarrollador web" />
    <Hero />
    <SobreMi />
    <ProyectosIndex show={4}/>
    
  </Layout>
)

export default IndexPage
