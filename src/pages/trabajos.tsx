import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import TarjetaTrabajo from "../components/UI/TarjetaTrabajo"
import Features from "../components/UI/ProyectoCaracteristica"

const Trabajos: React.FC = () => {
  const caracteristicasArriba = [
    [
      "Diseño a Maquetado",
      "Desarrollo de maquetado HTML, CSS y JavaScript partiendo de un diseño",
    ],
    [
      "Maquetado a WordPress",
      "Conversión de la web estática para ser compatible con WordPress",
    ],
    [
      "Proyectos desde cero",
      "Desarrollo de sitio sin Diseño previo hasta poder usarlo en WordPress",
    ],
    [
      "Desarrollo de Apps",
      "Trabajo de la estructura de apps para Android e IOS",
    ],
  ]

  return (
    <Layout>
      <SEO title="Trabajos" />
      <div className="page-wrapper">
        <div className="seccion">
          <div className="contenedor contenedor--fluid">
            <h2 className="contenedor__titulo">En qué he trabajado</h2>
            <Features items={caracteristicasArriba} />
            <div className="tarjetas-trabajos">
              <TarjetaTrabajo
                color="#4381C1"
                imagen="portafolio.jpg"
                titulo="Mi portafolio"
                descripcion="Desarrollé este portafolio con una tecnología que no conocía bien para aprender y me siento felíz del resultado. Este sitio lo desarrollé con GatsbyJs + ReactJs"
                enlace="https://javiermiz.gitlab.io/"
              />
              <TarjetaTrabajo
                color="#FEA100"
                imagen="airmobil.jpg"
                titulo="Airmobil"
                descripcion="Web para ofrecer servicios de perforaciones de pozos y excavaciones desde la que se puede pedir una cotización por medio de formularios. Sitio desarrollado con WordPress para aumentar las ventas de mi padre con buenos resultados"
                enlace="http://airmobil.com.mx/"
              />
              <TarjetaTrabajo
                color="#f0b7b3"
                imagen="bolsaskaty.jpg"
                titulo="Bolsas Katy"
                descripcion="Desarrollo web de página estílo e-commerce para venta de bolsas que enlaza los productos con Amazon, gracias a los afiliados a Amazon. Sitio desarrollado con WordPress + elementor como proyecto personal que actualmente no está activo"
                enlace="http://www.bolsaskaty.com"
              />
              <TarjetaTrabajo
                color="#FDA506"
                imagen="eclessia7.jpg"
                titulo="Ecclesia 7"
                descripcion="Desarrollo del front-end de app para dar diezmos y donaciones para la iglesia Adventista. Me encargue de desarrollar algunos componentes visuales de la app basados en un diseño"
              />
              <TarjetaTrabajo
                color="#FE1500"
                imagen="empenapp.png"
                titulo="Empeñapp"
                descripcion="Desarrollo del front-end de app para hacer empeños. Me encargué en solitario de desarrollar toda la estructura visual de la app sin un diseño previo haciendo los cambios pedidos por el cliente"
              />
              <TarjetaTrabajo
                color="#FF6E3B"
                imagen="la-jarochita.jpg"
                titulo="La Jarochita"
                descripcion="Desarrollo de rediseño del sitio de un comercio de comida partiendo de un diseño trabajado por otra persona. Maqueté completamente el HTML, CSS y Javascript para pasarlo después a WordPress con PHP y así facilitar su administración"
                enlace="http://www.lajarochita.mx"
              />
              <TarjetaTrabajo
                color="#9D489B"
                imagen="clinica-vida-fertil.jpg"
                titulo="Clínica vida fértil"
                descripcion="Desarrollo para ofrecer servicios de clínica y facilitar el contácto por correo o WhatsApp al igual que ofrecer la información en inglés y español. Sitio desarrollado con WordPress junto con otras 2 personas."
                enlace="https://clinicavidafertil.com/es/"
              />
              <TarjetaTrabajo
                color="#12A1DD"
                imagen="key-coronavirus.jpg"
                titulo="Key | Coronavirus"
                descripcion="Desarrolle el sitio en HTML, CSS y JS para posteriormente convertirlo en un tema de WordPress, administrable por medio de Advance custom Fields con PHP"
                enlace="https://key.com.mx/coronavirus/"
              />
              <TarjetaTrabajo
                color="#FF3567"
                imagen="dare-to-learn.jpg"
                titulo="Dare To Learn"
                descripcion="Este sitio se desarrolló con Wordpress + Divi. Rediseñando el sitio que se tenía anteriormente y trabajando el nuevo diseño con el constructor visual"
                enlace="https://daretolearn.com.mx/"
              />
              <TarjetaTrabajo
                color="#AB1F71"
                imagen="spa-medica.jpg"
                titulo="Spa medica | Dr Toledo"
                descripcion="Desarrollado con ayuda de otra persona, este sitio manejé la maquetación con Wordpress + Divi y provee la información en 2 idiomas, español e inglés"
                enlace="https://www.drtoledospamedica.com/inicio/"
              />
              <TarjetaTrabajo
                color="#0846CD"
                imagen="industronic.jpg"
                titulo="Grupo Industronic"
                descripcion="En este proyecto ayudé a maquetar algunos elementos del sitio y hacerlos administrables con WordPress. Por ejemplo la navegación, la página de servicios, etc"
                enlace="https://grupoindustronic.com/"
              />
              <TarjetaTrabajo
                color="#F2D283"
                imagen="gusmarcos-contacto.jpg"
                titulo="Gus Marcos | Contacto"
                descripcion="Se maquetó la página de contácto completamente para el sítio de Gus Marcos para luego hacerla administrable por medio de Wordpress"
                enlace="https://gusmarcos.mx/contacto/"
              />
              <TarjetaTrabajo
                color="#D49F09"
                imagen="flatworld.jpg"
                titulo="Flatworld"
                descripcion="Como proyecto personal y por diversión desarrollé un sito (no oficial) para un juego en desarrollo hecho por Guinxu (Youtuber) enteramente en HTML, CSS y Javascript"
                enlace="http://airmobil.com.mx/javiermiz/flatworld/"
              />
              <TarjetaTrabajo
                color="#FD6408"
                imagen="natura.jpg"
                titulo="Cliente Natura"
                descripcion="Web desarrollada para un cliente personal para impulsar su negocio. Desarrollada enteramente sin diseño previo con Wordpress + Elementor"
              />
              <TarjetaTrabajo
                color="#ff8200"
                imagen="latitudmx.jpg"
                titulo="Latitud Mx"
                descripcion="Maquetación enteramente en HTML, CSS y Javascript partiendo de un diseño."
              />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default Trabajos
